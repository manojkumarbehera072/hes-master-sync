﻿namespace TPDDL.HESMasterSync.Models
{
    class EquipmentInventoryInput
    {
        public string RefID { get; set; }
        public string EquipmentID { get; set; }
        public string AltequipmentID { get; set; }
        public string EquipmentMake { get; set; }
        public string EquipmentMaterialNumber { get; set; }
        public string EquipmentType { get; set; }
        public string SimSerialNumber { get; set; }
        public string NicSerialNumber { get; set; }
        public string SimMobileNo { get; set; }
        public string SimIPAddress { get; set; }
        public string MeteringMode { get; set; }
    }
}
