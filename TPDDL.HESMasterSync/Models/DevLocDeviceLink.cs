﻿using System;

namespace TPDDL.HESMasterSync.Models
{
    public class DevLocDeviceLink
    {
        public DateTime InstallationDate { get; set; }
        public DateTime RemovalDate { get; set; }
    }
}
