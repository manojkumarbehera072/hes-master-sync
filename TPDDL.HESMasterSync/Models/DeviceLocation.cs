﻿namespace TPDDL.HESMasterSync.Models
{
    public class DeviceLocation
    {
        public string GPS_LAT { get; set; }
        public string GPS_LONG { get; set; }
        public string Utility_Office_ID { get; set; }
    }
}
