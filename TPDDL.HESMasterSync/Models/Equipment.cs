﻿namespace TPDDL.HESMasterSync.Models
{
    public class Equipment
    {
        public string Meter_ID { get; set; }
        public string Utility_ID { get; set; }
        public string Manufacturer_ID { get; set; }
        public string Material_ID { get; set; }
        public string Meter_Type { get; set; }
    }
}
