﻿using System;

namespace TPDDL.HESMasterSync.Models
{
    class EquipmentActivityInput
    {
        public string RefID { get; set; }
        public string EquipmentID { get; set; }
        public DateTime InstallationDate { get; set; }
        public DateTime RemovalDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string ConsumerID { get; set; }
        public string OfficeID { get; set; }
        public string ConnStatus { get; set; }
        public string ConsumerType { get; set; }
    }
}
