﻿using System;

namespace TPDDL.HESMasterSync.Models
{
    public class HesWorkFlow
    {
        public int Id { get; set; }
        public string Workflow_Type { get; set; }
        public string Refrence_Object_Type { get; set; }
        public string Reference_Obj_Id { get; set; }
        public string Workflow_Status { get; set; }
        public string Workflow_Response_Text { get; set; }
        public string Request_Sent_Time { get; set; }
        public string Response_Received_Time { get; set; }
        public int HES_Id { get; set; }
        public string Data_Source { get; set; }
        public int Retry_Count { get; set; }
        public DateTime Created_On { get; set; }
        public string Created_By { get; set; }
        public DateTime Changed_On { get; set; }
        public string Changed_By { get; set; }
    }
}
