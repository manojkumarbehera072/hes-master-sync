﻿using System;
using System.Collections.Generic;
using System.Linq;
using TPDDL.HESMasterSync.Helpers;
using TPDDL.HESMasterSync.Models;

namespace TPDDL.HESMasterSync
{
    class Program
    {
        static void Main(string[] args)
        {
            HesConnectorCall hesConnectorCall = new HesConnectorCall();
            HesMasterSync hesMasterSync = new HesMasterSync();
            List<HesWorkFlow> workFlowList = new List<HesWorkFlow>();
            workFlowList = hesMasterSync.GetHesWorkFlow();
            if (workFlowList != null)
            {
                var workFlows = workFlowList.GroupBy(x => new { x.Workflow_Type }).ToList();

                foreach (var workFlow in workFlows)
                {
                    if (workFlow.Key.Workflow_Type == "EquipmentInventory")
                    {
                        EquipmentInventory equipmentInventory = new EquipmentInventory();

                        List<EquipmentInventoryInput> equipmentInventoryInputList = new List<EquipmentInventoryInput>();
                        foreach (var EquipmentInventory in workFlow)
                        {
                            Equipment equipment = equipmentInventory.GetEquipmentByID(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string simSerialNumber = equipmentInventory.GetSimSerialNo(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string nicSerialNumber = equipmentInventory.GetNicSimSerialNo(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string simMobileNo = equipmentInventory.GetSimMobileNo(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string simIPAddress = equipmentInventory.GetSimIPAddress(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string equipmentingMode = equipmentInventory.GetEquipmentingMode(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));


                            EquipmentInventoryInput equipmentInventoryInput = new EquipmentInventoryInput();
                            equipmentInventoryInput.RefID = Convert.ToString(EquipmentInventory.Id);
                            equipmentInventoryInput.EquipmentID = equipment.Meter_ID;
                            equipmentInventoryInput.AltequipmentID = equipment.Utility_ID;
                            equipmentInventoryInput.EquipmentMake = equipment.Manufacturer_ID;
                            equipmentInventoryInput.EquipmentType = equipment.Meter_Type;
                            equipmentInventoryInput.EquipmentMaterialNumber = equipment.Material_ID;
                            equipmentInventoryInput.SimSerialNumber = simSerialNumber;
                            equipmentInventoryInput.NicSerialNumber = nicSerialNumber;
                            equipmentInventoryInput.SimMobileNo = simMobileNo;
                            equipmentInventoryInput.SimIPAddress = simIPAddress;
                            equipmentInventoryInput.MeteringMode = equipmentingMode;

                            equipmentInventoryInputList.Add(equipmentInventoryInput);
                        }
                        hesConnectorCall.CallCreateEquipmentInventory(equipmentInventoryInputList);

                    }

                    if (workFlow.Key.Workflow_Type == "EquipmentActivity")
                    {
                        List<EquipmentActivityInput> equipmentActivityInputList = new List<EquipmentActivityInput>();

                        foreach (var EquipmentInventory in workFlow)
                        {
                            EquipmentActivity equipmentActivity = new EquipmentActivity();
                            EquipmentActivityInput equipmentActivityInput = new EquipmentActivityInput();


                            string meterID = equipmentActivity.GetMeterIDByID(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            DevLocDeviceLink devLocDeviceLink = equipmentActivity.GetDevLocDeviceLink(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            DeviceLocation deviceLocation = equipmentActivity.GetDeviceLocationByEquipmentID(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string consumerID = equipmentActivity.GetConsumerIDByID(Convert.ToInt32(EquipmentInventory.Reference_Obj_Id));

                            string connStatus = equipmentActivity.GetConnStatus();

                            string consumerType = equipmentActivity.GetConsumerType();

                            equipmentActivityInput.RefID = Convert.ToString(EquipmentInventory.Id);
                            equipmentActivityInput.EquipmentID = meterID;
                            equipmentActivityInput.InstallationDate = devLocDeviceLink.InstallationDate;
                            equipmentActivityInput.RemovalDate = devLocDeviceLink.RemovalDate;
                            equipmentActivityInput.Latitude = deviceLocation.GPS_LAT;
                            equipmentActivityInput.Longitude = deviceLocation.GPS_LONG;
                            equipmentActivityInput.ConsumerID = consumerID;
                            equipmentActivityInput.OfficeID = deviceLocation.Utility_Office_ID;
                            equipmentActivityInput.ConnStatus = connStatus;
                            equipmentActivityInput.ConsumerType = consumerType;

                            equipmentActivityInputList.Add(equipmentActivityInput);
                        }

                        hesConnectorCall.CallUpdateEquipmentActivity(equipmentActivityInputList);
                    }

                }
            }
        }
    }
}
