﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.HESMasterSync.Models;

namespace TPDDL.HESMasterSync.Helpers
{
    public class EquipmentInventory
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;
        public Equipment GetEquipmentByID(int equipmentID)
        {
            Equipment equipment = new Equipment();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select EQ.Meter_ID,EQ.Utility_ID,EQ.Manufacturer_ID,EQ.Material_ID,mm.Meter_Type from Equipment EQ inner join [Material_Master] MM " +
                        "on EQ.Material_ID = MM.Utility_ID where EQ.ID =" + equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        equipment.Meter_ID = reader["Meter_ID"] == DBNull.Value ? "null" : reader["Meter_ID"].ToString();
                        equipment.Utility_ID = reader["Utility_ID"] == DBNull.Value ? "null" : reader["Utility_ID"].ToString();
                        equipment.Manufacturer_ID = reader["Manufacturer_ID"] == DBNull.Value ? "null" : reader["Manufacturer_ID"].ToString();
                        equipment.Material_ID = reader["Material_ID"] == DBNull.Value ? "null" : reader["Material_ID"].ToString();
                        equipment.Meter_Type = reader["Meter_Type"] == DBNull.Value ? "null" : reader["Meter_Type"].ToString();
                    }

                    connection.Close();
                    return equipment;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetSimSerialNo(int equipmentID)
        {
            string simSerialNo = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT EA.Attrib_Value FROM [dbo].[Attrib_Names] AN INNER JOIN[dbo].[Equipment_Attrib] EA ON AN.ID = EA.Attrib_ID " +
                        "LEFT OUTER JOIN[dbo].[Equipment] EQ ON EA.Equipment_ID = EQ.ID WHERE AN.Attrib_Name = 'simSerialNumber' " +
                        "AND(EA.Valid_To is NULL or EA.Valid_To = '9999-12-31') AND EQ.ID = "+ equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        simSerialNo = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return simSerialNo;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetSimMobileNo(int equipmentID)
        {
            string simSerialNo = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT EA.Attrib_Value FROM [dbo].[Attrib_Names] AN INNER JOIN[dbo].[Equipment_Attrib] EA ON AN.ID = EA.Attrib_ID " +
                        "LEFT OUTER JOIN[dbo].[Equipment] EQ ON EA.Equipment_ID = EQ.ID WHERE AN.Attrib_Name = 'SimMobileNo' " +
                        "AND(EA.Valid_To is NULL or EA.Valid_To = '9999-12-31') AND EQ.ID = " + equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        simSerialNo = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return simSerialNo;
            }
            catch (Exception)
            {

                throw;
            }

        }


        public string GetNicSimSerialNo(int equipmentID)
        {
            string nicSerialNumber = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT EA.Attrib_Value FROM [dbo].[Attrib_Names] AN INNER JOIN[dbo].[Equipment_Attrib] EA ON AN.ID = EA.Attrib_ID " +
                        "LEFT OUTER JOIN[dbo].[Equipment] EQ ON EA.Equipment_ID = EQ.ID WHERE AN.Attrib_Name = 'NicSerialNumber' " +
                        "AND(EA.Valid_To is NULL or EA.Valid_To = '9999-12-31') AND EQ.ID = " + equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        nicSerialNumber = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return nicSerialNumber;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetSimIPAddress(int equipmentID)
        {
            string SimIPAddress = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT EA.Attrib_Value FROM [dbo].[Attrib_Names] AN INNER JOIN[dbo].[Equipment_Attrib] EA ON AN.ID = EA.Attrib_ID " +
                        "LEFT OUTER JOIN[dbo].[Equipment] EQ ON EA.Equipment_ID = EQ.ID WHERE AN.Attrib_Name = 'SimIPAddress' " +
                        "AND(EA.Valid_To is NULL or EA.Valid_To = '9999-12-31') AND EQ.ID = " + equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        SimIPAddress = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return SimIPAddress;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetEquipmentingMode(int equipmentID)
        {
            string equipmentingMode = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "SELECT EA.Attrib_Value FROM [dbo].[Attrib_Names] AN INNER JOIN[dbo].[Equipment_Attrib] EA ON AN.ID = EA.Attrib_ID " +
                        "LEFT OUTER JOIN[dbo].[Equipment] EQ ON EA.Equipment_ID = EQ.ID WHERE AN.Attrib_Name = 'MeteringMode' " +
                        "AND(EA.Valid_To is NULL or EA.Valid_To = '9999-12-31') AND EQ.ID = " + equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        equipmentingMode = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return equipmentingMode;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
