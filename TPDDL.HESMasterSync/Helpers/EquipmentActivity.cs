﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.HESMasterSync.Models;

namespace TPDDL.HESMasterSync.Helpers
{
    public class EquipmentActivity
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;

        public string GetMeterIDByID(int equipmentID)
        {
            string meterID = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select Meter_ID from Equipment where ID="+equipmentID+";";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        meterID = reader["Meter_ID"] == DBNull.Value ? "null" : reader["Meter_ID"].ToString();
                    }
                    connection.Close();
                }
                return meterID;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public DevLocDeviceLink GetDevLocDeviceLink(int equipmentId)
        {
            DevLocDeviceLink devLocDeviceLink = new DevLocDeviceLink();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select max(ValidFromDate) as InstallationDate,max(ValidToDate) as RemovalDate  from [dbo].[DevLoc_Device_Link] WHERE EquipmentId = " + equipmentId + " ;";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        devLocDeviceLink.InstallationDate = reader["InstallationDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["InstallationDate"]);
                        devLocDeviceLink.RemovalDate = reader["RemovalDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["RemovalDate"]);                        
                    }

                    connection.Close();
                    return devLocDeviceLink;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public DeviceLocation GetDeviceLocationByEquipmentID(int equipmentID)
        {
            DeviceLocation deviceLocation = new DeviceLocation();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select DL.GPS_LAT,DL.GPS_LONG,DL.Utility_Office_ID from [dbo].[Device_Location] DL inner join " +
                        "Equipment EQ on DL.Utility_ID = EQ.Utility_ID where EQ.ID = "+ equipmentID + "";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        deviceLocation.GPS_LONG = reader["GPS_LONG"] == DBNull.Value ? "null" : reader["GPS_LONG"].ToString();
                        deviceLocation.GPS_LAT = reader["GPS_LAT"] == DBNull.Value ? "null" : reader["GPS_LAT"].ToString();
                        deviceLocation.Utility_Office_ID = reader["Utility_Office_ID"] == DBNull.Value ? "null" : reader["Utility_Office_ID"].ToString();
                    }

                    connection.Close();
                    return deviceLocation;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetConsumerIDByID(int equipmentID)
        {
            string consumerID = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select CDL.ConsumerID from [dbo].[ConsumerDevLocLink] CDL inner join " +
                        "[dbo].[Device_Location] DL on CDL.DeviceLocationId = DL.ID inner join " +
                        "Equipment EQ on DL.Utility_ID = EQ.Utility_ID where EQ.ID = "+ equipmentID + "; ";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        consumerID = reader["ConsumerID"] == DBNull.Value ? "null" : reader["ConsumerID"].ToString();
                    }
                    connection.Close();
                }
                return consumerID;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetConnStatus()
        {
            string connStatus = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select EA.Attrib_Value from [dbo].[Attrib_Names] AN inner join [dbo].[Consumer_Attrib] EA on AN.ID = EA.ID " +
                        "where AN.Attrib_Name = 'ConnectionStatus'; ";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        connStatus = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return connStatus;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public string GetConsumerType()
        {
            string consumerType = "";
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select EA.Attrib_Value from [dbo].[Attrib_Names] AN inner join [dbo].[Consumer_Attrib] EA on AN.ID = EA.ID " +
                        "where AN.Attrib_Name = 'ConsumerType'; ";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        consumerType = reader["Attrib_Value"] == DBNull.Value ? "null" : reader["Attrib_Value"].ToString();
                    }
                    connection.Close();
                }
                return consumerType;
            }
            catch (Exception)
            {

                throw;
            }

        }

    }
}
