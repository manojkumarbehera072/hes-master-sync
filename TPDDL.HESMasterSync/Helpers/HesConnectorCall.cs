﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using TPDDL.HESMasterSync.Models;

namespace TPDDL.HESMasterSync.Helpers
{
    class HesConnectorCall
    {
        public static string hesConnectorUrl = ConfigurationManager.AppSettings["hesConnectorUrl"];

        public void CallCreateEquipmentInventory(List<EquipmentInventoryInput> equipmentInventoryInputList)
        {
            var body = "";
            try
            {
                var client = new RestClient(hesConnectorUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Host", "localhost");
                request.AddHeader("Content-Type", "text/xml; charset=utf-8");
                request.AddHeader("SOAPAction", "http://tatapower-uds.org/CreateEquipmentInventory");
                body = @"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                <soap:Body>
                    <CreateEquipmentInventory xmlns=""http://tatapower-uds.org/"">
                        <equipmentInventoryInput>
                            <MessageHeader>
                                <MessageID>string</MessageID>
                                <DateTime>2021-12-12T07:31:10Z</DateTime>
                            </MessageHeader>
                            <MessageBody>
                                <Equipment>
                                " + EquipmentInventoryArray(equipmentInventoryInputList) + @"
                                </Equipment>
                            </MessageBody>
                        </equipmentInventoryInput>
                    </CreateEquipmentInventory>
                </soap:Body>
                </soap:Envelope>";
                request.AddParameter("text/xml; charset=utf-8", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                WriteLog("CallCreateEquipmentInventory" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), body);
            }
            catch (Exception)
            {
                WriteLog("CallCreateEquipmentInventory" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), body);
                throw;
            }
        }
        public void CallUpdateEquipmentActivity(List<EquipmentActivityInput> equipmentActivityInputList)
        {
            var body = "";
            try
            {
                var client = new RestClient(hesConnectorUrl);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Host", "localhost");
                request.AddHeader("Content-Type", "text/xml; charset=utf-8");
                request.AddHeader("SOAPAction", "http://tatapower-uds.org/UpdateEquipmentActivity");
                body = @"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
                <soap:Body>
                    <UpdateEquipmentActivity xmlns=""http://tatapower-uds.org/"">
                        <equipmentActivityInput>
                            <UpdateMessageHeader>
                                <MessageID>string</MessageID>
                                <DateTime>2021-12-12T07:31:10Z</DateTime>
                            </UpdateMessageHeader>
                            <UpdateMessageBody>
                                <UpdateEquipment>
                                       " + EquipmentActivityArray(equipmentActivityInputList) + @"
                                </UpdateEquipment>
                            </UpdateMessageBody>
                        </equipmentActivityInput>
                    </UpdateEquipmentActivity>
                </soap:Body>
                </soap:Envelope>";
                request.AddParameter("text/xml; charset=utf-8", body, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                Console.WriteLine(response.Content);
                WriteLog("CallUpdateEquipmentActivity" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), body);

            }
            catch (Exception)
            {
                WriteLog("CallUpdateEquipmentActivity" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss"), body);
                throw;
            }

        }

        private string EquipmentInventoryArray(List<EquipmentInventoryInput> equipmentInventoryInputList)
        {
            string text = "";

            foreach (var equipmentInventoryInput in equipmentInventoryInputList)
            {
                text += @"<Equipment>
                                        <RefID>" + equipmentInventoryInput.RefID + @"</RefID>
                                        <EquipmentID>" + equipmentInventoryInput.EquipmentID + @"</EquipmentID>
                                        <AltequipmentID>" + equipmentInventoryInput.AltequipmentID + @"</AltequipmentID>
                                        <EquipmentMake>" + equipmentInventoryInput.EquipmentMake + @"</EquipmentMake>
                                        <EquipmentMaterialNumber>" + equipmentInventoryInput.EquipmentMaterialNumber + @"</EquipmentMaterialNumber>
                                        <EquipmentType>" + equipmentInventoryInput.EquipmentType + @"</EquipmentType>
                                        <SimSerialNumber>" + equipmentInventoryInput.SimSerialNumber + @"</SimSerialNumber>
                                        <NicSerialNumber>" + equipmentInventoryInput.NicSerialNumber + @"</NicSerialNumber>
                                        <SimMobileNo>" + equipmentInventoryInput.SimMobileNo + @"</SimMobileNo>
                                        <SimIPAddress>" + equipmentInventoryInput.SimIPAddress + @"</SimIPAddress>
                                        <MeteringMode>" + equipmentInventoryInput.MeteringMode + @"</MeteringMode>
                                    </Equipment>";
            }

            return text;
        }

        private string EquipmentActivityArray(List<EquipmentActivityInput> equipmentActivityInputList)
        {
            string text = "";

            foreach (var equipmentActivityInput in equipmentActivityInputList)
            {
                text += @"<UpdateEquipment>
                                        <RefID>" + equipmentActivityInput.RefID + @"</RefID>
                                        <EquipmentID>" + equipmentActivityInput.EquipmentID + @"</EquipmentID>
                                        <InstallationDate>" + equipmentActivityInput.InstallationDate.ToString("yyyy-MM-dd") + @"</InstallationDate>
                                        <RemovalDate>" + equipmentActivityInput.RemovalDate.ToString("yyyy-MM-dd") + @"</RemovalDate>
                                        <Latitude>" + equipmentActivityInput.Latitude + @"</Latitude>
                                        <Longitude>" + equipmentActivityInput.Longitude + @"</Longitude>
                                        <ConsumerID>" + equipmentActivityInput.ConsumerID + @"</ConsumerID>
                                        <OfficeID>" + equipmentActivityInput.OfficeID + @"</OfficeID>
                                        <ConnStatus>" + equipmentActivityInput.ConnStatus + @"</ConnStatus>
                                        <ConsumerType>" + equipmentActivityInput.ConsumerType + @"</ConsumerType>
                                    </UpdateEquipment>";
            }

            return text;
        }

        private static void WriteLog(string strFileName, string strMessage)
        {
            try
            {

                string path = Directory.GetCurrentDirectory();
                string parent = Directory.GetParent(path).FullName;
                string payloadPath = Directory.GetParent(parent).FullName + "\\HesConnectorCall_Payload\\";
                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", payloadPath, strFileName+".txt"), FileMode.Append, FileAccess.Write);
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(strMessage);
                objStreamWriter.Close();
                objFilestream.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
