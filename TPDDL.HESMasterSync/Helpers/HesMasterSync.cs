﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using TPDDL.HESMasterSync.Models;

namespace TPDDL.HESMasterSync.Helpers
{
    class HesMasterSync
    {
        SqlDataReader reader;
        public static string constr = ConfigurationManager.ConnectionStrings["MDMConnectionString"].ConnectionString;

        public List<HesWorkFlow> GetHesWorkFlow()
        {
            List<HesWorkFlow> hesWorkFlowsList = new List<HesWorkFlow>();
            try
            {
                using (SqlConnection connection = new SqlConnection(constr))
                {
                    connection.Open();
                    string query = "select * from [HES_WORKFLOW] where ([Workflow_Status]=1 or [Workflow_Status]=4 )and [Retry_Count]<=(select CONFIG_VALUE from CONFIG_DEVICE_CONTROLS where CONFIG_NAME  ='NumberofRetriesforHES');";
                    SqlCommand cmd = new SqlCommand(query, connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        HesWorkFlow hesWorkFlow = new HesWorkFlow();

                        hesWorkFlow.Id = reader["id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["id"].ToString());
                        hesWorkFlow.Workflow_Type = reader["Workflow_Type"] == DBNull.Value ? "null" : reader["Workflow_Type"].ToString();
                        hesWorkFlow.Refrence_Object_Type = reader["Refrence_Object_Type"].ToString();
                        hesWorkFlow.Reference_Obj_Id = reader["Reference_Obj_Id"] == DBNull.Value ? "null" : reader["Reference_Obj_Id"].ToString();
                        hesWorkFlow.Workflow_Status = reader["Workflow_Status"] == DBNull.Value ? "null" : reader["Workflow_Status"].ToString();
                        hesWorkFlow.Workflow_Response_Text = reader["Workflow_Response_Text"] == DBNull.Value ? "null" : reader["Workflow_Response_Text"].ToString();
                        hesWorkFlow.Request_Sent_Time = reader["Request_Sent_Time"] == DBNull.Value ? "null" : reader["Request_Sent_Time"].ToString();
                        hesWorkFlow.Response_Received_Time = reader["Response_Received_Time"] == DBNull.Value ? "null" : reader["Response_Received_Time"].ToString();
                        hesWorkFlow.HES_Id = reader["HES_Id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["HES_Id"].ToString());
                        hesWorkFlow.Data_Source = reader["Data_Source"] == DBNull.Value ? "null" : reader["Data_Source"].ToString();
                        hesWorkFlow.Retry_Count = reader["Retry_Count"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Retry_Count"].ToString());
                        hesWorkFlow.Created_On = reader["Created_On"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["Created_On"].ToString());
                        hesWorkFlow.Created_By = reader["Created_By"] == DBNull.Value ? "null" : reader["Created_By"].ToString();
                        hesWorkFlow.Changed_On = reader["Changed_On"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["Changed_On"].ToString());
                        hesWorkFlow.Changed_By = reader["Changed_By"] == DBNull.Value ? "null" : reader["Changed_By"].ToString();
                        hesWorkFlowsList.Add(hesWorkFlow);
                    }

                    connection.Close();
                    return hesWorkFlowsList;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
      
    }
}
